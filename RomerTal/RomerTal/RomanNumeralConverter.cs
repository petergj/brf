using System;
using System.Collections.Generic;

namespace RomerTal
{
    public class RomanNumeralConverter
    {
        private static IDictionary<int, string> romanValuePairs = new Dictionary<int, string>()
                                                                      {
                                                                          {1000, "M"},
                                                                          {900, "CM"},
                                                                          {500, "D"},
                                                                          {400, "CD"},
                                                                          {100, "C"},
                                                                          {90, "XC"},
                                                                          {50, "L"},
                                                                          {40, "XL"},
                                                                          {10, "X"},
                                                                          {9, "IX"},
                                                                          {5, "V"},
                                                                          {4, "IV"},
                                                                          {1, "I"}
                                                                      };

        public static int Convert(string romanNumreal)
        {
            foreach (var romanValuePair in romanValuePairs)
            {
                if (romanNumreal.StartsWith(romanValuePair.Value))
                    return romanValuePair.Key + Convert(romanNumreal.Substring(romanValuePair.Value.Length));
            }

            return 0;
        }

        public static string Convert(int decimalNumber)
        {
            foreach (var romanValuePair in romanValuePairs)
            {
                if (decimalNumber >= romanValuePair.Key)
                    return romanValuePair.Value + Convert(decimalNumber-romanValuePair.Key);
            }

            return "";
        }
    }
}