﻿#language: da-DK
Egenskab:  Man skal kunne konvertere alle heltal fra 1 til og med 3000 til romertal

Baggrund:
	Givet at et romertal altid består af følgende bogstaver
	| Bogstav | Værdi |
	| I       | 1     |
	| V       | 5     |
	| X       | 10    |
	| L       | 50    |
	| C       | 100   |
	| D       | 500   |
	| M       | 1000  |
	Og et hvilket som helst decimal tal fra 1 til og med 3000
	

Scenarie: Konvertering af decimaltal til romertal
	Når jeg konverterer det til romertal
	Så vil jeg få et romertal

Scenarie: I et hvilket som helst romertal må ethvert bogstav kun optræde tre gange i 
	Når jeg konverterer det til romertal
	Så vil et bogstav aldrig optræde mere end tre gange i træk

Scenarie: I et hvilket som helst romertal er der bogstaver, som kun optræder højst en gang
	Givet et hvilket som helst decimal tal fra 1 til og med 3000
	Når jeg konverterer det til romertal
	Så vil følgende bogstaver højst optræde en gang
	| Bogstav |
	| V       |
	| L       |
	| D       |

Scenarie: I et hvilket som helst romertal, vil der kun stå ét mindre romertal før et større
	Givet et hvilket som helst decimal tal fra 1 til og med 3000
	Når jeg konverterer det til romertal
	Så vil der højst stå et mindre romertal før et større romertal

Scenarie: I et romertal er der besteme bogstavkombinationer, som aldrig må optræde
	Givet et hvilket som helst decimal tal fra 1 til og med 3000
	Når jeg konverterer det til romertal
	Så er vil følgende bogstaver aldrig efterfølges af andet end
	| Bogstav | Må Efterfølges Af |
	| I       | V                 |
	| I       | X                 |
	| X       | L                 |
	| X       | C                 |
	| C       | D                 |
	| C       | M                 |
	
Scenarie: Konvertering af romertal tilbage til decimaltal
	Givet jeg konverterer det til romertal
	Når jeg konverterer det tilbage til decimaltal
	Så vil jeg få det samme decimaltal