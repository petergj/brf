﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace RomerTal.Specs.Steps
{
    [Binding]
    public class RomertalSteps
    {
        private IEnumerable<int> _numbersToConvert;
        private Dictionary<string, int> _basicRomanNumeralsMap;
        private Dictionary<int, string> _decimalToRomanMap;
        private Dictionary<string, int> _romanToDecimalMap;
        private List<string> _basicRomanNumerals;

        [BeforeScenario()]
        public void BeforeScenario()
        {
            _numbersToConvert = null;
            _basicRomanNumeralsMap = null;
            _decimalToRomanMap = null;
        }

        [Given(@"at et romertal altid består af følgende bogstaver")]
        public void GivetRomertalAltidBestarAfFolgendeBogstaver(Table table)
        {
            _basicRomanNumeralsMap = table.CreateSet<Romertal>().ToDictionary(r => r.Bogstav, r => r.Værdi);
            _basicRomanNumerals = _basicRomanNumeralsMap.Keys.ToList();
        }

        [Given(@"et hvilket som helst decimal tal fra (.*) til og med (.*)")]
        public void GivetEtHvilketSomHelstDecimalTalFraTilOgMed(int start, int slut)
        {
            _numbersToConvert = Enumerable.Range(start, slut);
        }

        [Given(@"jeg konverterer det til romertal")]
        [When(@"jeg konverterer det til romertal")]
        public void NarJegKonvertererDetTilRomertal()
        {
            _decimalToRomanMap = new Dictionary<int, string>();

            foreach (var decimalNumber in _numbersToConvert)
            {
                _decimalToRomanMap.Add(decimalNumber, RomanNumeralConverter.Convert(decimalNumber));
            }
        }

        [Then(@"vil jeg få et romertal")]
        public void SaVilJegFaEtRomertal()
        {
            foreach (var roman in _decimalToRomanMap.Values)
            {
                var actual = roman;
                _basicRomanNumerals.ForEach(r => actual = actual.Replace(r, ""));

                Assert.That(actual, Is.EqualTo(""));
            }
        }

        [Then(@"vil følgende bogstaver højst optræde en gang")]
        public void SaVilFolgendeBogstaverHojstOptraedeEnGang(Table table)
        {
            var bogstaverTilladtEnGang = table.CreateSet<Romertal>().Select(r => r.Bogstav.ToCharArray().First()).ToList();

            foreach (var romanNumeral in _decimalToRomanMap.Values)
            {
                bogstaverTilladtEnGang.ForEach(
                    bogstav => Assert.That(romanNumeral.Count(c => c == bogstav), Is.LessThanOrEqualTo(1)));
            }
        }

        [Then(@"vil et bogstav aldrig optræde mere end tre gange i træk")]
        public void SaVilEtBogstavAldrigOptraedeMereEndTreGangeITraek()
        {
            foreach (var romanNumeral in _decimalToRomanMap.Values)
            {
                AssertIlligalCombinations(romanNumeral, _basicRomanNumerals.Select(br => br + br + br + br));
            }
        }

        [Then(@"vil der højst stå et mindre romertal før et større romertal")]
        public void SaVilDerHojstStaEtMindreRomertalForEtStorreRomertal()
        {
            foreach (var romanNumeral in _decimalToRomanMap.Values)
            {
                AssertThatOnlyOneSmallerRomanNumeralIsBeforeALargerOne(romanNumeral);
            }
        }

        [When(@"jeg konverterer det tilbage til decimaltal")]
        public void NarJegKonvertererDetTilbageTilDecimaltal()
        {
            _romanToDecimalMap = new Dictionary<string, int>();

            foreach (var romanNumeral in _decimalToRomanMap.Values)
            {
                _romanToDecimalMap.Add(romanNumeral, RomanNumeralConverter.Convert(romanNumeral));
            }
        }

        [Then(@"vil jeg få det samme decimaltal")]
        public void SaVilJegFaDetSammeDecimaltal()
        {
            foreach (var decimalPair in _decimalToRomanMap)
            {
                Assert.That(_romanToDecimalMap.ContainsKey(decimalPair.Value));
                Assert.That(_romanToDecimalMap[decimalPair.Value], Is.EqualTo(decimalPair.Key));
            }
        }

        [Then(@"er vil følgende bogstaver aldrig efterfølges af andet end")]
        public void SaErVilFolgendeBogstaverAldrigEfterfolgesAfAndetEnd(Table table)
        {

            var ulovligeParKombinationer = GetIlligalRomanNumeralCombinations(table.CreateSet<Romertal>());

            foreach (var romanNumeral in _decimalToRomanMap.Values)
            {
                AssertIlligalCombinations(romanNumeral, ulovligeParKombinationer);
            }
        }

        private IList<string> GetIlligalRomanNumeralCombinations(IEnumerable<Romertal> romertal)
        {
            var romertalKombinationer = romertal.GroupBy(r => r.Bogstav)
                .Select(grp => new
                                   {
                                       Bogstav = grp.Key,
                                       MåEfterFølgesAf = grp.Select(g => g.MåEfterfølgesAf).ToList()
                                   });

            return romertalKombinationer.SelectMany(rk => _basicRomanNumerals.Except(rk.MåEfterFølgesAf)
                                                              .Select(br => rk + br)).ToList();
        }

        private void AssertThatOnlyOneSmallerRomanNumeralIsBeforeALargerOne(string romanNumeral)
        {
            var romanValues = romanNumeral.ToArray().Select(c => _basicRomanNumeralsMap[c.ToString()]).Reverse();

            var numberOfSmallerNumbers = 0;
            int previousValue = 0;
            foreach (var value in romanValues)
            {
                if (value >= previousValue)
                {
                    // Set everytime a new larger numbers was met.
                    previousValue = value;
                    numberOfSmallerNumbers = 0;
                }
                else
                {
                    Assert.That(numberOfSmallerNumbers, Is.EqualTo(0));

                    // Count the number of smaller numbers, we meet in a row
                    numberOfSmallerNumbers++;
                }
            }
        }

        private void AssertIlligalCombinations(string romanNumeral, IEnumerable<string> illigalCombinations)
        {
            foreach (var illigalCombination in illigalCombinations)
            {
                Assert.That(romanNumeral.Contains(illigalCombination), Is.False);
            }
        }

        public class Romertal
        {
            public string Bogstav { get; set; }
            public int Værdi { get; set; }
            public string MåEfterfølgesAf { get; set; }
        }
    }
}
