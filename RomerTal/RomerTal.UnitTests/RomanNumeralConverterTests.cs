﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace RomerTal.UnitTests
{
    [TestFixture]
    public class RomanNumeralConverterTests
    {
        [TestCase(1, "I")]
        [TestCase(5, "V")]
        [TestCase(10, "X")]
        [TestCase(50, "L")]
        [TestCase(100, "C")]
        [TestCase(500, "D")]
        [TestCase(1000, "M")]
        public void ShouldConvertBasicNumbers(int baseDecimal, string expectedRomanNumeral)
        {
            ConvertAndAssert(baseDecimal, expectedRomanNumeral);
        }
        
        [TestCase(4, "IV")]
        [TestCase(9, "IX")]
        [TestCase(40, "XL")]
        [TestCase(90, "XC")]
        [TestCase(400, "CD")]
        [TestCase(900, "CM")]
        public void SholdConvertTheAllowedTwoLetterPairs(int baseDecimalPair, string expectedRomanNumeral)
        {
            ConvertAndAssert(baseDecimalPair, expectedRomanNumeral);
        }

        [TestCase(1)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(9)]
        [TestCase(10)]
        [TestCase(40)]
        [TestCase(50)]
        [TestCase(90)]
        [TestCase(100)]
        [TestCase(400)]
        [TestCase(500)]
        [TestCase(900)]
        [TestCase(1000)]
        public void ShouldConvertRomanNumeralsBackToOriginalDecimal(int decimalNumber)
        {
            // Act
            var romanNumber = RomanNumeralConverter.Convert(decimalNumber);
            var actualDecimal = RomanNumeralConverter.Convert(romanNumber);

            // Assert
            Assert.That(actualDecimal, Is.EqualTo(decimalNumber));
        }

        [Test]
        public void ShouldReturnEmptyStringOnNegativeNumber()
        {
            // Act
            var actual = RomanNumeralConverter.Convert(-10);

            // Assert
            Assert.That(actual, Is.EqualTo(""));
        }
      
        private static void ConvertAndAssert(int baseDecimal, string expectedRomanNumeral)
        {
            // Act
            var actual = RomanNumeralConverter.Convert(baseDecimal);

            // Assert
            Assert.That(actual, Is.EqualTo(expectedRomanNumeral));
        }

    }
}
